# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.32
# 

Name:       arrowboard-de

# >> macros
# << macros

Summary:    German and Custom Qwertz Keyboards with Arrows
Version:    1.3
Release:    2
Group:      System/I18n
License:    MIT
BuildArch:  noarch
URL:        https://openrepos.net/content/nephros/german-keyboard-arrows
Source0:    %{name}-%{version}.tar.gz
Source100:  arrowboard-de.yaml
Requires:   %{name}-de
Requires:   %{name}-qwertz

%description
German Keyboard with Arrows and Custom Qwertz with Arrows


%package qwertz
Summary:    Custom Qwertz Keyboard with Arrows
Group:      System/I18n
Requires:   arrowboard-common

%description qwertz
%{summary}.

%package de
Summary:    German Keyboard with Arrows
Group:      System/I18n
Requires:   arrowboard-common

%description de
%{summary}.

%prep
%setup -q -n %{name}-%{version}

# >> setup
# << setup

%build
# >> build pre
# << build pre



# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre

# >> install post
for f in maliit/plugins/com/jolla/layouts/*; do
%{__install} -m 644 -D ${f}  $RPM_BUILD_ROOT%{_datadir}/${f}
done
# << install post

%files
%defattr(-,root,root,-)
# >> files
# << files

%files qwertz
%defattr(-,root,root,-)
%{_datadir}/maliit/plugins/com/jolla/layouts/custom-qwertz_2arw.conf
%{_datadir}/maliit/plugins/com/jolla/layouts/custom-qwertz_2arw.qml
# >> files qwertz
# << files qwertz

%files de
%defattr(-,root,root,-)
%{_datadir}/maliit/plugins/com/jolla/layouts/de_2arw.conf
%{_datadir}/maliit/plugins/com/jolla/layouts/de_2arw.qml
%{_datadir}/maliit/plugins/com/jolla/layouts/de_4arw.conf
%{_datadir}/maliit/plugins/com/jolla/layouts/de_4arw.qml
%{_datadir}/maliit/plugins/com/jolla/layouts/de_ios.qml
%{_datadir}/maliit/plugins/com/jolla/layouts/de_ios.conf
# >> files de
# << files de
